import seaborn as sns
from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

sns.set(style="whitegrid", palette='deep')
sns.set_context("paper")


Re = 4
Le = 10e-5
Rem = 10
Lem = 1.9e-3
Cem = 0.13e-3

Bl = 13.7
S = 2000
M = 203

f = np.linspace(50, 40000, int(10e5))
f1 = np.linspace(100, 1000, int(10e3))
f2 = np.linspace(900, 40000, int(10e3))


Cer = ((Bl)**2)/((S**2)*M)

w = 2*np.pi*f

z = Re + (1j*w*Le) + ((1j*w*Lem)/(1 + (1j*w*(Lem/Rem)) - (Cem + Cer)*(Lem*w**2)))

Z = np.abs(z)

#################################################
w1 = 2*np.pi*f1
z1 = Re + ((1j*w1*Lem)/(1 + (1j*w1*(Lem/Rem)) - (Cem + Cer)*(Lem*w1**2)))

#################################################
w2 = 2*np.pi*f2
z2 = Re + (Le*w2)
#################################################

# fr = (1 / 2 * np.pi) * 1 / (np.sqrt(Le * (Cem + Cer)))
# print(fr)

plt.plot(f, Z)
plt.plot(f1, z1, '--')
plt.plot(f2, z2, '--')
# plt.vlines(fr, 0, 100, colors='purple', linestyles='dashdot')
plt.hlines(4, 50, 40000, colors='purple', linestyles='dashdot')
# plt.title('Impedance')
plt.xlabel('Fréquence (Hz)')
plt.ylabel('Impedance')
plt.xscale('log')
plt.grid(True, which="both", ls="--", c="gray")
plt.show()
